/*
    Demonstrates shadowing, scope, and variable declarations. Prints out:

    37
    loop i = 0
    loop i = 1
    loop i = 2
    37

*/

#include <stdio.h>

int main()
{
    unsigned int i = 37;

    printf("%i\n", i);

    for(unsigned int i = 0; i<3; i++)
    {
        printf("loop i = %i\n", i);
    }

    printf("%i\n", i);

    return 0;
}


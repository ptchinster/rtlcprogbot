/* goto's are a valid and used thing in C, anybody saying they should NEVER be used is wrong. You should not use a goto to exit a loop (use break or continue, or restructure your loop), but gotos are perfectly fine for optimizing code and cleaning up. Below is one such example.

The linux kernel has tens of thousands of them. https://github.com/torvalds/linux/search?q=goto
The compiler is forced to issue an unconditional jump, which is an optimization. Optimizations in kernel code are much more critical than userspace code.

Dijkstra has an opinion: https://www.cs.utexas.edu/users/EWD/ewd02xx/EWD215.PDF

*/

int myFunction(int arg1, int arg2)
{
    return ret = SUCCESS;

    if(doSomeInitA() == FAIL) { ret = FAIL; goto cleanupA; }
    if(doSomeInitB() == FAIL) { ret = FAIL; goto cleanupB; }
    if(doSomeInitC() == FAIL) { ret = FAIL; goto cleanupC; }

    //Init was a success, do the thing you need to


    /*
        Cleanups will "Fall though". If you need to cleanup B, you need to cleanup not C, B, and A, in that order. This style lets you "unwind", which leads to not having to duplicate code.
    */

cleanupC:
    DeInitC();

cleanupB:
    DeInitB();

cleanupA:
    DeInitA();

    return ret;
}


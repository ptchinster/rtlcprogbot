//*************************************************************************************************
/*
    This is to demonstrate that floats (and doubles) will have odd behaviors compared to what beginners may expect.
    It is generally best practice to use ints where possible (for example, int dollar and int cents, and every time you add/subtract
    from cents, you do any borrowing from dollar

    For more information, read IEEE 754 (https://en.wikipedia.org/wiki/IEEE_754) and check out https://0.30000000000000004.com/

    This also was discussed here: https://www.reddit.com/r/cprogramming/comments/men0vi/trouble_with_an_if_statement_condition/

*/

#include <stdio.h>
#include <math.h> //Make sure you compile with -lm

int main()
{

    float f = round(256.49999); //stored as 256.49999000000003
    printf("%f\n", f); //prints 256

    f = roundf(256.49999); //stored as 256.5
    printf("%f\n", f); //prints 257


    float d = 0.0;
    for(d = 0.0 ; d != 0.3; d += 0.1)
    {
        printf("%f\n", d);
    }

    return 0;
}
//*************************************************************************************************



commands = {
    "!scanf": "[A Beginners Guide Away From scanf](http://sekrit.de/webdocs/c/beginners-guide-away-from-scanf.html).\n\n" + 
                "[scanf is unsafe for int input](https://wiki.sei.cmu.edu/confluence/plugins/servlet/mobile?contentId=87152392#content/view/87152392)",

    "!homework2": "We are not here to do your homework for you. Come with a question, have made an attempt, show your source code, and be ready to have a discussion, try, and learn. Use something like [GitLab](https://gitlab.com/) to share your attempt so far.",

    "!homework": "When asking for help with your homework, please post contact information for your teacher so the Reddit community can ask for clarification on your assignment.",

    "!sizeofchar": "Section 6.5.3.4: When applied to an operand that has type char, unsigned char, or signed char, (or a qualified version thereof) the result is 1. \n\n" +
                    "The number of bits in a char is CHAR_BIT bits. \n\n" +
                    "A byte was and still is defined as the smallest addressable unit. \n\n" +
                    "A char is at least 8 bits (C89 §2.2.4.2.1; C90 §5.2.4.2.1). An int is at least 16 bits. The  Control Data 6600 used a 60-bit word, and it was the smallest addressable unit. Essentially, it had a 60 bit byte. The DEC-10 had arbitrary sized bytes (with a 36 bit word). Some embedded systems have sizeof(char) == sizeof(short) == sizeof(int) == 16.\n\n" +
                    "[Fixed-width integer types](https://en.wikipedia.org/wiki/C_data_types#stdint.h)",


    "!undef":   "[What every C programmer should know about undefined behavior](http://blog.llvm.org/2011/05/what-every-c-programmer-should-know.html)\n\n" +
                "[A Guide to Undefined Behavior in C, part 1](https://embeddedartistry.com/blog/2017/01/09/a-guide-to-undefined-behavior-in-c-and-c-part-1/) \n\n" +
                "[C Language: Undefined Behavior](https://riptutorial.com/c/topic/364/undefined-behavior)\n\n" +
                "[C99 Undefined Behavior](https://gist.github.com/Earnestly/7c903f481ff9d29a3dd1)"
,

    "!malloc":  "[Windows doc on malloc. Check the return value!](https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/malloc?view=msvc-160) \n\n" +
                "[What they dont tell you about demand paging](https://offlinemark.com/2020/10/14/demand-paging/)",

    "!macros": "[How do do macros the right way](https://www.pixelstech.net/article/1390482950-do-%7B-%7D-while-%280%29-in-macros).",

    "!semaphore": "[The Little Book of Semaphores](https://greenteapress.com/wp/semaphores/)",

    "!flags": "[Useful gcc flags](https://stackoverflow.com/questions/3375697/useful-gcc-flags-for-c) \n\n" +
              "[Compiler and linker flags for gcc](https://developers.redhat.com/blog/2018/03/21/compiler-and-linker-flags-gcc/)" +
              "[gcc manpage](http://manpages.org/gcc)",


    "!smash": "[Smashing The Stack For Fun And Profit](https://inst.eecs.berkeley.edu/~cs161/fa08/papers/stack_smashing.pdf)",

    "!uaf": "[Use-After-Free aka UAF](https://cwe.mitre.org/data/definitions/416.html)",

    "!doublefree": "[Double Free](https://cwe.mitre.org/data/definitions/415.html)",

    "!wwwcompile": "Check out some online compilers. You can also share your source to help troubleshoot issues. \n\n" +
                   "[Online gdb](https://www.onlinegdb.com/online_c_compiler) \n\n" +
                   "[repl it](https://repl.it/languages/c)",

    "!cdecl":   "[C gibberish -> English Translator](https://cdecl.org/)",

    "!valgrind":    "[Valgrind: A Quick Start](https://www.valgrind.org/docs/manual/quick-start.html) \n\n" +
                    "[Valgrind Suppression File Howto](https://wiki.wxwidgets.org/Valgrind_Suppression_File_Howto) \n\n" +
                    "[Callgrind: a call-graph generating cache and branch prediction profiler](https://valgrind.org/docs/manual/cl-manual.html)",

    "!beej":    "[Beej's Guide to Network Programming](https://beej.us/guide/bgnet/)",

    "!fflushstdin": "Flushing stdin is undefined behavior in the C language. [It is wrong on both Windows and Linux](https://faq.cprogramming.com/cgi-bin/smartfaq.cgi?answer=1052863818&id=1043284351). [This](https://faq.cprogramming.com/cgi-bin/smartfaq.cgi?answer=1044873249&id=1043284392) is the proper way.",

    "!init": "[Always](https://cwe.mitre.org/data/definitions/457.html). [Initialize](https://www.blackhat.com/presentations/bh-europe-06/bh-eu-06-Flake.pdf). [Your](https://msrc-blog.microsoft.com/2008/03/11/ms08-014-the-case-of-the-uninitialized-stack-variable-vulnerability/). [Variables.](https://dev.to/idanarye/dont-initialize-your-variables-40d) [Heres how](https://en.cppreference.com/w/c/language/array_initialization).",

    "!tutorial": "[Basic C Programming](https://computer.howstuffworks.com/c.htm/printable)",

    "!bigo": "[Know your BigO!](https://www.bigocheatsheet.com/)",

    "!rtlcprogbot": "[What is rtlcprogbot?](https://www.reddit.com/r/cprogramming/comments/k0ydlo/whats_this_rtlcprogbot_im_seeing/)",

    "!debugger": "[GDB Tutorial](https://www.tutorialspoint.com/gnu_debugger/index.htm) \n\n" +
                "[WinDbg](https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/getting-started-with-windbg) \n\n" +
                "[MS Visual Studio Debugger](https://docs.microsoft.com/en-us/visualstudio/debugger/debugger-feature-tour?view=vs-2019)",

    "!whitetext": "Check your post. I have to highlight your text to read it (white back ground). Im browsing from the latest version of Chrome.",

    "!c++":  "This is C++ code, not C. They are 2 different languages. Try /r/cpp",

    "!make": "[Learn to use a Makefile](http://web.mit.edu/gnu/doc/html/make_2.html)",

    "!picoftext": "For the love of fuck dont give us a picture of text.",

    "!vla": "[Legitimate Use of Variable Length Arrays](https://nullprogram.com/blog/2019/10/27/)\n\n" +
            "[The too many pitfalls of VLA](https://blog.joren.ga/programming/vla-bad)",

    "!format": "Please take a look at reddit formatting and format your code. You could also share a gitlab or online compiler URL. [https://content.nunction.net/tools/redditlint/](https://content.nunction.net/tools/redditlint/) is also great.",

    "!cheating": "Please PM /u/ptchinster a brief essay explaining why cheating hurts you, your school, your fellow (and past) students from your school, your future employers, future coworkers, and future customers.",

    "!constvolatile": "[Difference between const and const volatile](https://stackoverflow.com/questions/4592762/difference-between-const-const-volatile)",

    "!volatile": "[Should a variable used with multi-threads be volatile?](https://stackoverflow.com/questions/45663059/volatile-keyword-with-mutex-and-semaphores#45663377)\n\n" +
                 "[Volatile is not needed if you are using locks properly](https://stackoverflow.com/questions/78172/using-c-pthreads-do-shared-variables-need-to-be-volatile/78221#78221)\n\n" +
                 "[Volatile: Almost Useless for Multi-Threaded Programming](http://web.archive.org/web/20190219170904/https://software.intel.com/en-us/blogs/2007/11/30/volatile-almost-useless-for-multi-threaded-programming/)",


    "!barrier": "[Hardware vs Software Barriers](https://stackoverflow.com/questions/19965076/gcc-memory-barrier-sync-synchronize-vs-asm-volatile-memory)\n\n" +
                "[Memory Ordering in Modern Microprocessors, Part I](http://web.archive.org/web/20181011003451/https://www.linuxjournal.com/article/8211)",

    "!unicode": "[Understanding text for C Programmers (UTF-8, Unicode, ASCII](https://www.youtube.com/watch?v=70b9ineDgLU)",


#TODO i know this is cpp but https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md

#TODO need to organize, by topic?

#TODO !unsafe, a list of unsafe functions and what to use instead

#TODO link to the ways you can perform IPC

}


#!/usr/bin/env python3

import praw
import os
import sys
import signal
import getopt
import gc
from tempfile import gettempdir
from time import sleep, time
from fcntl import lockf, LOCK_EX, LOCK_NB
from os.path import join

# from systemd.daemon import notify as systemddaemonnotify TODO
from commands import *
from users import *
from subreddits import *

# TODO i noticed that on 1 post, my user replied with 2 posts with commands and only the first one was replied to. need to replicate.
# https://www.reddit.com/r/cprogramming/comments/oft3in/the_great_ancient_debate_pretty_funny/h4fu66y/

# TODO doesnt appear to catch new top level posts, just comments. change this.

# TODO add signal handler for USR1 and do a gc

# TODO fix homework2 also triggering homework command

# TODO mutex doesnt seem to work on exit


sourcecodedir = "./sources/"  # relative to this script
userAgent = "rtlcprogbot"
cID = "Enter your personal use script"
cSC = "Enter you client secret"
reddit = None
subreddit = None
MUTEX_PATH = None
MUTEX_LOCK = None
DEBUG = 0
helpflag = 0
abort = 0
isUnique = True
disclaimer = "\n\n*I am a bot. Replying to me notifies nobody*\n\n"
ratelimit = 3  # max number of posts per ratetime
ratetime = 0.5  # in hours
userposts = {}  # k:v dictionary of user:posts in last hour


def signal_handler(sig, frame):
    global abort
    abort = 1
    print("\nCaught signal! Exiting.\n")
    sleep(2)  # let main finish anything its doing
    cleanup()
    sys.exit(0)
    # loop in main will exit to go to cleanup


def init_account():
    global reddit
    global subreddit
    global cSC
    global cID

    try:
        f = open(os.path.dirname(os.path.realpath(__file__)) + "/" + "account.txt", "r")
        lines = f.read().splitlines()
        username = lines[0]
        userpassword = lines[1]
        cSC = lines[2]
        cID = lines[3]
        f.close()
    except:
        print(
            "Problem reading account.txt. Needs at least 4 lines in this order:\n\tusername\n\tpassword\n\tClient Secret\n\tPersonal Use Script\n"
        )
        cleanup()
        sys.exit(-1)

    try:
        reddit = praw.Reddit(
            user_agent=userAgent,
            client_id=cID,
            client_secret=cSC,
            username=username,
            password=userpassword,
        )
        subs = "+".join(monitorsubs)
        subreddit = reddit.subreddit(subs)
    except:
        print(
            "Error connecting, maybe your creds are wrong or your internet connection is down?\n"
        )
        cleanup()
        sys.exit(-1)


# action = -1: decrement by 1
# action = >0: increment by 1
def updatePostCount(username, action):
    global userposts

    print(f"In updatePostCount with {username} and action {action}")

    if username not in userposts:
        userposts[username] = 0

    if action < 0:
        if userposts[username] > 0:
            userposts[username] -= 1
    else:
        userposts[username] += 1

    print(f"{username} now has {userposts[username]} posts")

    if userposts[username] == 0:
        del userposts[username]


# returns True if user is under rate limit OR an authorized user
# returns False if user is at or above rate limit
def rateLimitCheck(username):
    global userposts
    global ratelimit

    if username not in userposts:
        userposts[username] = 0

    if username in auth_users:
        return True
    elif userposts[username] >= ratelimit:
        return False
    else:
        return True


def scanMessages():
    global DEBUG
    global ratetime
    global abort

    # for comment in subreddit.stream.comments(skip_existing=True):
    try:
        tic = time()
        for comment in subreddit.stream.comments():
            commentauthor = str(comment.author)
            if rateLimitCheck(commentauthor) and commentauthor != userAgent:
                print(f"Caught a post by an authorized user {commentauthor}")
                replystr = ""
                doReply = 1

                for cmd in commands.keys():
                    # print("\t!--%s--!\n" % comment.body)
                    if cmd in comment.body:
                        # print("\tFound command |||%s|||\n" % cmd)
                        comment.refresh()
                        forest = comment.replies
                        for tree in forest:
                            if tree.author == userAgent:
                                print("\tSkipping")
                                doReply = 0
                        if doReply:
                            replystr += "**%s**\n\n" % cmd
                            replystr += "%s" % commands[cmd]
                            replystr += "\n\n"
                        else:
                            break

                if doReply and len(replystr):
                    replystr += disclaimer
                    if DEBUG:
                        print("\tREPLYING!")
                        print("WOULD REPLY - \n%s\n" % replystr)
                        updatePostCount(commentauthor, 1)
                    else:
                        comment.reply(replystr)
                        updatePostCount(commentauthor, 1)
                        gc.collect()
            else:
                if commentauthor != userAgent:
                    print(f"User {commentauthor} exceeds rate limit")
            toc = time()
            if (toc - tic) >= (60 * 60 * ratetime):
                print("TIMER!")
                tic = toc
                for u in userposts:
                    updatePostCount(u, -1)
            if abort:
                print("CAUGHT ABORT, NEED TO BREAK")
                break

    except Exception as e:
        print(f"Geneeral Exception caught: {e}\n")
    finally:
        sleep(1)


def slurp(filepath):
    ret = ""
    if os.path.exists(filepath):
        f = open(filepath, "r")
        lines = f.readlines()
        f.close()
        for line in lines:
            ret += "    " + line  # 4 spaces equal reddit markup for code
    else:
        print("Error loading %s!" % filepath)
        sys.exit(-1)
    return ret


def unique():
    global abort
    global MUTEX_LOCK
    global MUTEX_PATH
    global isUnique

    MUTEX_PATH = join(gettempdir(), "rtlcprogbot.lck")

    try:
        MUTEX_LOCK = open(MUTEX_PATH, "w")
        lockf(MUTEX_LOCK, LOCK_EX | LOCK_NB)
    except IOError:
        abort = 1
        isUnique = False
        print("Only 1 instance of this program can be running at a time.")
        sys.exit(-1)


def cleanup():
    try:
        gc.set_threshold(800, 20, 20)
        gc.enable()
        if isUnique:
            os.remove(MUTEX_PATH)
    except Exception as e:
        print(f"Exception caught: {e}")


def loadSources(srcdir):
    for filename in os.listdir(srcdir):
        if filename.endswith(".c"):
            base = os.path.basename(filename)
            base = os.path.splitext(base)[0]
            newcommand = "!" + base
            commands[newcommand] = slurp(srcdir + filename)


def make_help_look_nice(keylist):
    ret = "\n\nCommands for the bot include:\n\n"

    for k in keylist:
        ret += "* " + k + "\n"

    ret += "\n\n"

    return ret


def gchandler(phase, info):
    print("\tgchandler | %s | %s" % (phase, info))
    print("\t%s" % str(gc.get_count()))


def main():
    gc.disable()
    gc.set_threshold(0, 0, 0)
    gc.callbacks.append(gchandler)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    # signal.signal(signal.SIGUSR1, signal_handler)
    os.nice(19)

    loadSources(os.path.dirname(os.path.realpath(__file__)) + "/" + sourcecodedir)

    commands["!help"] = make_help_look_nice(commands.keys())

    if helpflag:
        print(commands["!help"])
        sys.exit(0)

    unique()

    init_account()
    gc.collect()
    # systemddaemonnotify('READY=1') #TODO
    while not abort:
        scanMessages()
    cleanup()


if __name__ == "__main__":
    try:
        opts, arg = getopt.getopt(sys.argv[1:], "h")
    except getopt.GetoptError as err:
        print(err)
        sys.exit(-1)
    for o, a in opts:
        if o == "-h":
            helpflag = 1

    main()
